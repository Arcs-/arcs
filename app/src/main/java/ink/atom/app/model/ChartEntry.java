package ink.atom.app.model;

/**
 * Created by About  23 Ninjas on 07.02.2016.
 */
public class ChartEntry {

    private String id;
    private String title;
    private String cover;

    public ChartEntry(String id, String title, String cover) {
        this.id = id;
        this.title = title;
        this.cover = cover;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
