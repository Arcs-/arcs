package ink.atom.app.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.*;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.Toast;
import ink.atom.app.Atom;
import ink.atom.app.R;
import ink.atom.app.helper.JsonTools;
import ink.atom.app.helper.NetworkUtil;
import org.acra.ACRA;
import org.blinkenlights.jid3.ID3Exception;
import org.blinkenlights.jid3.MP3File;
import org.blinkenlights.jid3.v2.APICID3V2Frame;
import org.blinkenlights.jid3.v2.ID3V2_3_0Tag;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;

public class ServiceDownload extends Service {

    private static final String NOTIFICATION_CANCEL_ID = "NOTIFICATION_CANCEL_ID";

    private static Handler handler;
    private static NotificationManagerCompat mNotifyManager;

    private static HashMap<String, AsyncDownload> currentDownloads;


    @Override
    public void onCreate() {
        super.onCreate();

        currentDownloads = new HashMap<>();

        handler = new Handler();
        mNotifyManager = NotificationManagerCompat.from(getApplicationContext());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.hasExtra(NOTIFICATION_CANCEL_ID)) {

            AsyncDownload download = currentDownloads.get(intent.getStringExtra(NOTIFICATION_CANCEL_ID));
            if(download != null) download.cancel(true);

        } else {

            String videoParam = intent.getExtras().getString("videoId");

            if (currentDownloads.get(videoParam) != null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Already downloading", Toast.LENGTH_LONG).show();
                    }
                });
                return 0;
            }

            long time = new Date().getTime();
            String tmpStr = String.valueOf(time);
            String last4Str = tmpStr.substring(tmpStr.length() - 5);

            final AsyncDownload downloadTask = new AsyncDownload(videoParam, Integer.valueOf(last4Str));
            currentDownloads.put(videoParam, downloadTask);
            downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        }
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {return null;}

    /**
     * to post toats from doInBackground
     *
     * @param runnable (thread to run toasts)
     */
    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

    /**
     * Downloads a File with given URL, create Notification and Get ID3 tags
     */
    private class AsyncDownload extends AsyncTask<Void, Integer, File> {

        private PowerManager.WakeLock mWakeLock;

        private NotificationCompat.Builder mBuilder;

        private int mNotificationID;
        private long mNotificationCreation;

        private String mVideoParam;
        private JSONObject mVideoInfo;

        AsyncDownload(String mVideoParam, int mNotificationID) {
            this.mVideoParam = mVideoParam;
            this.mNotificationID = mNotificationID;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            mWakeLock.acquire();

        }

        /**
         * Updates Notification
         *
         * @param values (current download status)
         */
        @Override
        protected void onProgressUpdate(Integer... values) {
            if (values == null || values.length == 0 || isCancelled()) return;

            mBuilder.setProgress(100, values[0], false);
            mNotifyManager.notify(mNotificationID, mBuilder.build());

            super.onProgressUpdate(values);
        }


        /**
         * Download a file and copy to final location
         *
         * @return downloaded file
         */
        @Override
        protected File doInBackground(Void... nothing) {

            if (!NetworkUtil.hasActiveInternetConnection(getApplicationContext())) {
                stopProcess(getString(R.string.error_no_network));
                return null;
            }

            Atom.getInstance().trackEvent("conversion", "youtube", mVideoParam);

            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;

            File mSongFile = null;

            try {

                /*
                    Prepare step
                 */

                final String mServerDownloadId = getInfo();
                createNotification();
                getImage();

                asyncHideNotification(mServerDownloadId);

                mSongFile = createOutputFile();

                /*
                    Download File
                 */


                URL url = new URL(getString(R.string.server_url) + "/api/proxy/" + mServerDownloadId + "/stream");
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    stopProcess(getString(R.string.error_connection));
                }

                // download the file
                input = connection.getInputStream();
                output = new FileOutputStream(mSongFile);

                byte data[] = new byte[4096];
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        mSongFile.delete();
                        stopProcess("Canceled");
                        return null;
                    }

                    output.write(data, 0, count);
                }




                /*
                    Finish work
                 */

                // set cover
                MP3File mp3File = new MP3File(mSongFile);

                ID3V2_3_0Tag oID3V2_3_0Tag = new ID3V2_3_0Tag();
                APICID3V2Frame mFrontCover = new APICID3V2Frame("image/jpg", APICID3V2Frame.PictureType.FrontCover, "", data);
                oID3V2_3_0Tag.addAPICFrame(mFrontCover);
                APICID3V2Frame mArtist = new APICID3V2Frame("image/jpg", APICID3V2Frame.PictureType.Artist, "", data);
                oID3V2_3_0Tag.addAPICFrame(mArtist);
                mp3File.setID3Tag(oID3V2_3_0Tag);

                oID3V2_3_0Tag.setTitle(mVideoInfo.optString("title"));
                oID3V2_3_0Tag.setArtist(mVideoInfo.optString("artist"));
                oID3V2_3_0Tag.setAlbum(mVideoInfo.optString("artist"));

                mp3File.sync();

                System.out.println("task id: " + mServerDownloadId + " done");

                return mSongFile;

            } catch (IOException e) {
                ACRA.getErrorReporter().handleException(e);
                stopProcess(getString(R.string.error_connection));
                e.printStackTrace();
            } catch (JSONException e) {
                ACRA.getErrorReporter().handleException(e);
                stopProcess(e.getMessage());
                e.printStackTrace();
            } catch (ID3Exception e) {
                ACRA.getErrorReporter().handleException(e);
                stopProcess(getString(R.string.error_corrupt));
                System.err.println("could not set ID3 tags ");
                e.printStackTrace();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();

                if(mSongFile != null)
                    mSongFile.delete();

            }

            return null;

        }

        private File createOutputFile() throws JSONException, IOException {
                            /* create file name */
            String subFolder = (mVideoInfo.getString("artist") != null) ? mVideoInfo.getString("artist") + '/' : "";

            String mMusicFolderPath;
            if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                // has sd card
                mMusicFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Music" + File.separator + subFolder;
            } else {
                mMusicFolderPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getAbsolutePath() + File.separator + subFolder;
            }

            File mMusicFolder = new File(mMusicFolderPath); //folder name
            mMusicFolder.mkdirs();

            File mSongFile = new File(mMusicFolder.getAbsolutePath() + File.separator + mVideoInfo.getString("title") + ".mp3");

            mSongFile.delete();


            return mSongFile;
        }

        private void asyncHideNotification(final String mServerDownloadId) {
            Thread showProgress = new Thread() {
                public void run() {
                    try {
                        int loops = 0;

                        while (!isCancelled() && !Thread.interrupted()) {
                            URL mPrepareUrl = new URL(getString(R.string.server_url) + "/api/proxy/" + mServerDownloadId);
                            JSONObject json = JsonTools.readObjectFromUrl(mPrepareUrl);

                            publishProgress(json.getInt("progress"));

                            // remove priority after 3 sec
                            if (++loops == 2) mBuilder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

                            if (json.getString("status").equals("done")) break;

                            Thread.sleep(500);

                        }

                    } catch (IOException | JSONException e) {
                        ACRA.getErrorReporter().handleException(e);
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        // Ignore
                    }

                }
            };
            showProgress.start();
        }

        private void getImage() throws JSONException, IOException {
            URL mImageURL = new URL(mVideoInfo.getString("cover"));
            if (!webFileExists(mImageURL)) mImageURL = new URL(getString(R.string.server_url) + "/default_cover.jpg");

            // Read the image ...
            InputStream inputStream = mImageURL.openStream();

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];

            int n;
            while (-1 != (n = inputStream.read(buffer))) output.write(buffer, 0, n);
            byte[] data = output.toByteArray();

            Bitmap cover = BitmapFactory.decodeByteArray(data, 0, data.length);
            cover = ThumbnailUtils.extractThumbnail(cover, 150, 150);
            mBuilder.setLargeIcon(cover);
            mNotifyManager.notify(mNotificationID, mBuilder.build());

        }

        private void createNotification() throws JSONException {
            Intent intent = new Intent(getApplicationContext(), ServiceDownload.class);
            intent.putExtra(NOTIFICATION_CANCEL_ID, mVideoParam);
            PendingIntent dismissIntent = PendingIntent.getService(getApplicationContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

            // notification
            mBuilder = new NotificationCompat.Builder(getApplicationContext());
            mBuilder.setContentTitle(mVideoInfo.getString("title"))
                    .setContentText("converting...")
                    .setTicker("Converting " + mVideoInfo.getString("title"))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setProgress(100, 0, false)
                    .setOngoing(true)
                    .addAction(R.drawable.ic_clear_black_24dp, "Cancel", dismissIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setWhen(mNotificationCreation);

            if (Build.VERSION.SDK_INT >= 21) mBuilder.setVibrate(new long[0]).setCategory(Notification.CATEGORY_PROGRESS);

            mNotifyManager.notify(mNotificationID, mBuilder.build());
        }

        /**
         * Create scan Intent and end notification
         *
         * @param mMusicFile downlaoded file
         */
        @Override
        protected void onPostExecute(File mMusicFile) {
            mWakeLock.release();

            if (mMusicFile == null) {
                mNotifyManager.cancel(mNotificationID);
                return;
            }

            Uri mFilePath = Uri.fromFile(mMusicFile);

            // Notification for media scanner
            Intent mMediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mMediaScanIntent.setData(mFilePath);
            sendBroadcast(mMediaScanIntent);

            // Notification for user
            mBuilder.setProgress(0, 0, false);

            Intent playIntent = new Intent();
            playIntent.setAction(Intent.ACTION_VIEW);
            playIntent.setDataAndType(mFilePath, "audio/*");

            playIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, playIntent, 0);

            mBuilder.setContentIntent(pi)
                    .setTicker(mVideoInfo.optString("title") + getString(R.string.fin_com))
                    .setContentText("done. Play it now!")
                    .setAutoCancel(true)
                    .setOngoing(false)
                    .setPriority(NotificationCompat.PRIORITY_HIGH);

            mBuilder.mActions.clear();

            mNotifyManager.notify(mNotificationID, mBuilder.build());

            new Thread() {
                public void run() {
                    try {
                        Thread.sleep(1500);

                        mBuilder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
                        mNotifyManager.notify(mNotificationID, mBuilder.build());

                    } catch (InterruptedException e) {
                        // Ignore
                    }
                }
            }.start();

            currentDownloads.remove(mVideoParam);
          //  stopSelf();

        }

        private String getInfo() throws IOException, JSONException {
            URL mPrepareUrl = new URL(getString(R.string.server_url) + "/api/prepare/" + mVideoParam);
            mVideoInfo = JsonTools.readObjectFromUrl(mPrepareUrl);
            if (mVideoInfo.optString("id").equals("")) {
                throw new JSONException(mVideoInfo.optString("message", "Mysterious error from server"));
            }

            String mServerDownloadId = mVideoInfo.getString("id");
            System.out.println("task id: " + mServerDownloadId);

            mNotificationCreation = System.currentTimeMillis();

            return mServerDownloadId;

        }

        /**
         * Cancel process
         *
         * @param mMessage (Error message)
         */
        private void stopProcess(final String mMessage) {
            if (mMessage != null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), mMessage, Toast.LENGTH_LONG).show();
                    }
                });
            }

            mNotifyManager.cancel(mNotificationID);
            this.cancel(true);
            currentDownloads.remove(mVideoParam);

        }

        private boolean webFileExists(URL url) {
            try {
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("HEAD");
                return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
    }

}