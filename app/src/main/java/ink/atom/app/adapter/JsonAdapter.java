package ink.atom.app.adapter;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import ink.atom.app.R;
import ink.atom.app.service.ServiceDownload;
import ink.atom.app.ui.DownloadActivity;
import ink.atom.app.ui.MainActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by About  23 Ninjas on 07.02.2016.
 */
public class JsonAdapter extends BaseAdapter implements ListAdapter {

    private ImageLoader imageLoader;

    private final Activity activity;
    private final JSONArray jsonArray;


    public JsonAdapter(Activity activity, JSONArray jsonArray) {
        if(jsonArray == null) this.jsonArray = new JSONArray();
        else this.jsonArray = jsonArray;
        this.activity = activity;

        this.imageLoader = ImageLoader.getInstance();
        this.imageLoader.init(ImageLoaderConfiguration.createDefault(activity.getBaseContext()));

    }


    @Override public int getCount() {

        return jsonArray.length();
    }

    @Override public JSONObject getItem(int position) {

        return jsonArray.optJSONObject(position);
    }

    @Override public long getItemId(int position) {
        JSONObject jsonObject = getItem(position);

        return jsonObject.optLong("id");
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) convertView = activity.getLayoutInflater().inflate(R.layout.list_charts, parent, false);

        final JSONObject jsonObject = getItem(position);

        TextView titleView = (TextView) convertView.findViewById(R.id.title);
        TextView artistView = (TextView) convertView.findViewById(R.id.artist);
        ImageView coverView = (ImageView) convertView.findViewById(R.id.cover);

        //ImageView convertButton = (ImageView) convertView.findViewById(R.id.overlay);

        try {
            titleView.setText(jsonObject.getString("title"));
            artistView.setText(jsonObject.getString("artist"));
            imageLoader.displayImage(jsonObject.getString("cover"), coverView);

            final String id = jsonObject.getString("id");

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(activity, DownloadActivity.class);
                    i.putExtra(Intent.EXTRA_TEXT, id);
                    i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    activity.startActivity(i);

                }
            });

        } catch (JSONException e) {
            titleView.setText("error");
        }



        return convertView;
    }

}
