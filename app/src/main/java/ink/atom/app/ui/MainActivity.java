package ink.atom.app.ui;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import ink.atom.app.R;
import ink.atom.app.adapter.JsonAdapter;
import ink.atom.app.helper.JsonTools;
import ink.atom.app.helper.NetworkUtil;
import ink.atom.app.helper.UpdateTools;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * Created by About  23 Ninjas on 30.01.2016.
 */
public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private ListView resultsView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View emptyView;
    private String initQuery;

    private HashMap<String, JSONArray> chartsCached;
    private MenuItem checkedMenuItem;

    private boolean lastWasSearch;
    private String lastKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultsView = (ListView) findViewById(R.id.charts);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        emptyView = findViewById(R.id.empty_view);

        chartsCached = new HashMap<String, JSONArray>(7);

        handleIntent(getIntent());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        // http://developer.android.com/training/search/setup.html
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                loadCharts("charts");
                return false;
            }
        });

        if(initQuery != null) {
            searchView.setQuery(initQuery, true);
            searchView.setIconified(false);
            searchView.clearFocus();
        }

        checkedMenuItem = menu.findItem(R.id.charts_option);
        checkMenuItem(checkedMenuItem);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.charts_option:
                checkMenuItem(item);
                loadCharts("charts");
                return true;
            case R.id.new_pop:
                checkMenuItem(item);
                loadCharts("new_pop");
                return true;
            case R.id.new_hiphop:
                checkMenuItem(item);
                loadCharts("new_hiphop");
                return true;
            case R.id.new_electro:
                checkMenuItem(item);
                loadCharts("new_electro");
                return true;
            case R.id.new_indie:
                checkMenuItem(item);
                loadCharts("new_indie");
                return true;
            case R.id.new_rock:
                checkMenuItem(item);
                loadCharts("new_rock");
                return true;
            case R.id.new_country:
                checkMenuItem(item);
                loadCharts("new_country");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        chartsCached.clear();

        if(lastWasSearch) loadResultsFor(lastKey);
        else loadCharts(lastKey);

    }

    private void checkMenuItem(MenuItem item) {
        checkedMenuItem.setEnabled(true);
        item.setEnabled(false);
        checkedMenuItem = item;
        setTitle(item.getTitle());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        UpdateTools.checkUpdateAsync(this, intent);

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            try {
                if (query == null || query.isEmpty()) loadCharts("charts");
                else loadResultsFor("http://atom.ink/api/search/" + URLEncoder.encode(query, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else if(Intent.ACTION_WEB_SEARCH.equals(intent.getAction())) {
            initQuery = intent.getStringExtra(SearchManager.QUERY);

        } else loadCharts("charts");
    }

    private void loadCharts(final String key) {
        lastWasSearch = false;
        lastKey = key;

        final Activity activity = this;
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {

                try {
                    if(chartsCached.get(key) == null && NetworkUtil.hasActiveInternetConnection(activity)) chartsCached.put(key, JsonTools.readArrayFromUrl(new URL(getString(R.string.server_url) + "/api/charts/" + key)));
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            resultsView.setAdapter(new JsonAdapter(activity, chartsCached.get(key)));
                            resultsView.setEmptyView(emptyView);

                            swipeRefreshLayout.setRefreshing(false);

                        }
                    });
                } catch(MalformedURLException e) {
                    // wont happen, URL is hardcoded
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e)  {
                    e.printStackTrace();
                }


            }
        });

        thread.start();

    }


    private void loadResultsFor(final String url) {
        lastWasSearch = true;
        lastKey = url;

        final Activity activity = this;
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    final JSONArray results;
                    if(NetworkUtil.hasActiveInternetConnection(activity)) results = JsonTools.readArrayFromUrl(new URL(url));
                    else results = null;

                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            resultsView.setAdapter(new JsonAdapter(activity, results));
                            resultsView.setEmptyView(emptyView);

                            swipeRefreshLayout.setRefreshing(false);
                        }
                    });

                } catch(MalformedURLException e) {
                    // wont happen, URL is hardcoded
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e)  {
                    e.printStackTrace();
                }




            }
        });

        thread.start();
    }

}