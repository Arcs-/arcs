package ink.atom.app.ui;

import android.Manifest;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import ink.atom.app.R;
import ink.atom.app.helper.NetworkUtil;
import ink.atom.app.helper.UpdateTools;
import ink.atom.app.service.ServiceDownload;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by About  23 Ninjas on 30.01.2016.
 */
public class DownloadActivity extends AppCompatActivity {

    private static final int WRITE_EXTERNAL_STORAGE_PERMISSION_CODE = 207;

    private String mVideoId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UpdateTools.checkUpdateAsync(this, this.getIntent());

        String mInputUrl = getIntent().getExtras().getString(Intent.EXTRA_TEXT);
        if (mInputUrl != null) {
            Pattern pattern = Pattern.compile("(/watch\\?v=[a-zA-Z0-9-_]{11})|(youtu\\.be/[a-zA-Z0-9-_]{11})|([a-zA-Z0-9-_]{11})");
            Matcher matcher = pattern.matcher(mInputUrl);
            if (matcher.find()) {
                mVideoId = matcher.group(0).substring(matcher.group(0).length() - 11);

                if(PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_PERMISSION_CODE);

                } else {
                    startDownload();
                }

            } else {
                Intent i = new Intent(this, MainActivity.class);
                i.setAction(Intent.ACTION_WEB_SEARCH);
                i.putExtra(SearchManager.QUERY, RMADVT(mInputUrl));
                i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);

            }

        }
    }

    private static String RMADVT(String orgurl) {
        System.out.println("org" + orgurl);
        if (orgurl.contains("Shazam")) {
			/* DE */if (orgurl.contains("Ich habe gerade erst durch Shazam")) {
                return orgurl.split("Shazam")[1].split("gefunden. h")[0]
                        .replace(" von ", " ");
            }
            /* DE+*/if (orgurl.contains("Ich habe gerade mit Shazam ")) {
                System.out.println("de + + " + orgurl.split("Ich habe gerade mit Shazam")[1]);
                return orgurl.split("Ich habe gerade mit Shazam ")[1].split("entdeckt. h")[0].replace(" von ", " by ");
            }
            /* EN+*/if (orgurl.contains("with Shazamn have a Listen:")) {
                return orgurl.split("discovered")[1].split("with Shazam,")[0];
            }
			/* EN */if (orgurl.contains("I just used Shazam")) {
                return orgurl.split("discover")[1].split(". ht")[0].replace(
                        " by ", " ");
            }
			/* SP */if (orgurl.contains("Acabo de utilizar Shazam")) {
                return orgurl.split("buscar")[1].split(". ht")[0].replace(
                        " de ", " ");
            }
			/* FR */if (orgurl.contains("Je viens d'utiliser Shazam")) {
                return orgurl.split("découvrir")[1].split(". ht")[0].replace(
                        " par ", " ");
            }

			/*
			if (orgurl.contains("ThISDonTExsist^^'")) {
                return orgurl.split("")[1].split("")[10].replace(" XX ", " ");
            }
            */
        }

        if (orgurl.contains("soundcloud.com")) {
            return orgurl.split("d.com/")[1].replaceAll("-", " ").replaceAll( "/", " ");
        }

        return orgurl;

    }

    private void startDownload() {
        Intent i = new Intent(this, ServiceDownload.class);
        i.putExtra("videoId", mVideoId);
        startService(i);
        finish();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startDownload();

                } else {
                    Toast.makeText(this, "Can't download a Song without the permission...", Toast.LENGTH_LONG).show();

                }
            }


        }
    }


}
