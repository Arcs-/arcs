package ink.atom.app.helper;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import ink.atom.app.BuildConfig;
import ink.atom.app.R;
import ink.atom.app.ui.MainActivity;
import org.acra.ACRA;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by About  23 Ninjas on 01.02.2016.
 */
public class UpdateTools {

    private static final String LINK_UPDATE_JSON = "/version.json";
    private static final String LINK_UPDATE_APK = "/arcs.apk";

    public static void checkUpdateAsync(final Activity activity, final Intent intent) {

        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {

                if(!NetworkUtil.hasActiveInternetConnection(activity)) return;

                if(intent.getExtras() != null && intent.getExtras().getBoolean("update", false)) UpdateTools.executeUpdate(activity);
                else UpdateTools.checkUpdate(activity);
            }
        });

        thread.start();

    }

    public static void checkUpdate(Context ctx) {

        try {
            JSONObject version = JsonTools.readObjectFromUrl(new URL(ctx.getString(R.string.server_url) + LINK_UPDATE_JSON));

            if (version.getInt("versionCode") > BuildConfig.VERSION_CODE) {

                System.out.println(version.getInt("versionCode"));

                Intent i = new Intent(ctx, MainActivity.class);
                i.putExtra("update", true);
                PendingIntent resultPendingIntent = PendingIntent.getActivity(ctx, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(ctx)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setLargeIcon(BitmapFactory.decodeResource(ctx.getResources(), R.mipmap.ic_launcher))
                                .setContentTitle("Update available")
                                .setTicker("Update available")
                                .setContentText("Update now to version " + version.getString("versionNumber"))
                                .setContentIntent(resultPendingIntent);

                NotificationManagerCompat mNotifyManager = NotificationManagerCompat.from(ctx);
                mNotifyManager.notify(1, mBuilder.build());

            }


        } catch (MalformedURLException e) {
            ACRA.getErrorReporter().handleException(e);
            e.printStackTrace();
        } catch (JSONException e) {
            ACRA.getErrorReporter().handleException(e);
            e.printStackTrace();
        } catch (IOException e) {
            ACRA.getErrorReporter().handleException(e);
            e.printStackTrace();
        }

    }


    private static void executeUpdate(Context ctx) {

        NotificationManagerCompat mNotifyManager = NotificationManagerCompat.from(ctx);
        mNotifyManager.cancel(1);

        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {

            File file = new File(ctx.getExternalCacheDir(), "arcs.apk");

            URL url = new URL(ctx.getString(R.string.server_url) + LINK_UPDATE_APK);
            inputStream = url.openStream();
            outputStream = new FileOutputStream(file);
            byte[] buffer = new byte[8192];

            int n;
            while (-1 != (n = inputStream.read(buffer))) outputStream.write(buffer, 0, n);

            inputStream.close();

            Intent promptInstall = new Intent(Intent.ACTION_VIEW).setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            ctx.startActivity(promptInstall);

        } catch (MalformedURLException e) {
            ACRA.getErrorReporter().handleException(e);
            e.printStackTrace();
        } catch (IOException e) {
            ACRA.getErrorReporter().handleException(e);
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }


}
